import java.io.*;
import java.net.*;

public class TCPClientAPP {
	private Socket tcpSocket;
	private PrintWriter out;
	private BufferedReader in;
	
	// constructor
	public TCPClientAPP(String host, int port){
		Socket tcpSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;
		try {
			tcpSocket = new Socket(host, port);
			out = new PrintWriter(tcpSocket.getOutputStream(), true);
			in = new BufferedReader(new InputStreamReader(tcpSocket.getInputStream()));
		} catch (UnknownHostException e) {
			System.err.println("Don't know about host: " + host);
			System.err.println("");
			System.exit(0);
		} catch (IOException e) {
			System.err.println("Couldn't get I/O for "+ "the connection to: " + host);
			System.err.println("");
			System.exit(0);
		}
		this.tcpSocket = tcpSocket;
		this.out = out;
		this.in = in;
	}
	public TCPClientAPP(){
		this("localhost", 1337);
	}
	public TCPClientAPP(String host){
		this(host, 1337);
	}

	public void print(Object s){
		System.out.print(s);
	}

	public void println(Object s){
		System.out.println(s);
	}

	public void speak() {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String userInput;

		print("$ ");

		try {
		
			userInput = stdIn.readLine();
			this.out.println(userInput);
			println("  > " + this.in.readLine());

			this.out.close();
			this.in.close();
			stdIn.close();
			this.tcpSocket.close();

		} catch (Exception e) {
			print(e);
		}
	}

	public void start() {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(System.in));
		String userInput;

		print("$ ");

		try {
		
			while ((userInput = stdIn.readLine()) != null) {
				this.out.println(userInput);
				println("  > " + this.in.readLine());
				print("$ ");
			}
		
			this.out.close();
			this.in.close();
			stdIn.close();
			this.tcpSocket.close();

		} catch (Exception e) {
			print(e);
		}
	}

}