import java.io.*;
import java.net.*;

public class UDPClientAPP {
	private int port;

	public UDPClientAPP(int port){
		this.port = port;
	}
	public UDPClientAPP(){
		this(1337);
	}
	
	public void speak() throws Exception {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		DatagramSocket clientSocket = new DatagramSocket();
		InetAddress ipAddress = InetAddress.getByName("localhost");
		
		byte[] sendData = new byte[1024];
		byte[] receiveData = new byte[1024];
		
		System.out.print("$ ");
		String sentence = inFromUser.readLine();
		sendData = sentence.getBytes();
		
		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, ipAddress, this.port);
		
		clientSocket.send(sendPacket);
		DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
		
		clientSocket.receive(receivePacket);
		
		String modifiedSentence = new String(receivePacket.getData());
		System.out.println(" > " + modifiedSentence);
		
		clientSocket.close();
	}
}