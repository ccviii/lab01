import java.net.*; 
import java.io.*; 

public class TCPServerAPP {
	private ServerSocket serverSocket;

	public TCPServerAPP(int port) {
		ServerSocket serverSocket = null; 

		try { 
			serverSocket = new ServerSocket(port); 
		} catch (IOException e) {
			System.err.println("\tCould not listen on port: "+port); 
			System.exit(1); 
		} 

		this.serverSocket = serverSocket;
	}

	public TCPServerAPP(){
		this(1337);
	}


	public void listen() throws Exception {
		Socket clientSocket = null; 
		System.out.println ("\tWaiting for connection.....");

		try { 
			clientSocket = serverSocket.accept(); 
		} catch (IOException e)  {
			System.err.println("Accept failed.");
			System.exit(1);
		}

		System.out.println ("\tConnection successful");
		System.out.println ("\tWaiting for input.....\n");

		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		BufferedReader in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));

		String inputLine;

		while ((inputLine = in.readLine()) != null) {
			// inputLine = in.readLine();
			System.out.println ("\tclient: " + inputLine);
			out.println(inputLine.toUpperCase()); 

			if (inputLine.equals(":q")) break; 
		}

		out.close(); 
		in.close(); 
		clientSocket.close(); 
		serverSocket.close(); 
	}
} 