public class App {
	public static void exit(){
		System.out.println("\nFaltan parametros!.");
		System.out.println("java App <tcp|upd> <client|server>");
	}
	public static void main(String[] args) throws Exception {

		if(args.length==2) {
			String type = args[0];
			String method = args[1];
			if(type.toUpperCase().equals("TCP")){
				if(method.toUpperCase().equals("CLIENT")){
					System.out.println(type+" "+method);
					TCPClientAPP client = new TCPClientAPP();
					client.speak();
				} else if(method.toUpperCase().equals("SERVER")) {
					System.out.println(type+" "+method);
					TCPServerAPP server = new TCPServerAPP();
					server.listen();
				} else {
					App.exit();
				}
			} else if(type.toUpperCase().equals("UDP")){
				if(method.toUpperCase().equals("CLIENT")){
					System.out.println(type+" "+method);
					UDPClientAPP client = new UDPClientAPP();
					client.speak();
				} else if(method.toUpperCase().equals("SERVER")) {
					System.out.println(type+" "+method);
					UDPServerAPP server = new UDPServerAPP();
					server.listen();
				} else {
					App.exit();
				}
			} else {
				App.exit();
			}
		} else if(args.length==3){
			TCPClientAPP client = new TCPClientAPP(args[0], Integer.parseInt(args[1]));
			client.start();
		} else {
			App.exit();
		}

		System.out.println("");
		
	}
}