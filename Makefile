make:
	clear
	javac TCPClientAPP.java
	javac TCPServerAPP.java
	javac UDPClientAPP.java
	javac UDPServerAPP.java
	javac App.java

clean:
	rm *.class
