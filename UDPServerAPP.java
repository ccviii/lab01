import java.io.*;
import java.net.*;

public class UDPServerAPP {
	private int port;
	private byte[] inBuffer, outBuffer;
	private static int size = 1024;
	private DatagramSocket serverSocket;

	public UDPServerAPP(int port) throws Exception {
		this.port = port;
		this.serverSocket = new DatagramSocket(port);
		this.inBuffer = new byte[size];
		this.outBuffer = new byte[size];
	}

	public UDPServerAPP() throws Exception {
		this(1337);
	}

	public void listen() throws Exception {
		// while(true) {
			DatagramPacket receivePacket = new DatagramPacket(inBuffer, inBuffer.length);
			serverSocket.receive(receivePacket);
			String sentence = new String( receivePacket.getData());
			System.out.println(" > " + sentence);
			InetAddress IPAddress = receivePacket.getAddress();
			int port = receivePacket.getPort();
			String capitalizedSentence = sentence.toUpperCase();
			outBuffer = capitalizedSentence.getBytes();
			DatagramPacket sendPacket = new DatagramPacket(outBuffer, outBuffer.length, IPAddress, port);
			serverSocket.send(sendPacket);
			serverSocket.close();
		// }
	}
}